TEX = pdflatex -interaction nonstopmode
BIB = bibtex
GS = gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite 

PAPER = main
BIBFILE = lib.bib
BUNDLE = out.pdf

all:	$(PAPER).pdf 
	$(GS) -sOutputFile=$(BUNDLE) $(PAPER).pdf 

view:	$(BUNDLE)
	open $(BUNDLE)

spell::
	ispell *.tex

clean::
	rm -fv *.aux *.log *.bbl *.blg *.toc *.out *.lot *.lof $(PAPER).pdf $(BUNDLE)

$(PAPER).pdf: $(PAPER).tex $(PAPER).bbl 
	$(TEX) $(PAPER) 
	$(TEX) $(PAPER)
	
$(PAPER).bbl: $(PAPER).tex $(BIBFILE)
	$(TEX) $(PAPER)
	$(BIB) $(PAPER)
